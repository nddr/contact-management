import { Accounts } from 'meteor/accounts-base';

Accounts.onLogout(() => {
    this.$router.push('/');
});
