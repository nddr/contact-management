import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods({
    createAdmin(email, password) {
        return Accounts.createUser({
            email,
            password,
            profile: {
                role: 'admin'
            }
        });
    }
});
