import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Accounts.onLoginFailure(() => {
	console.log('Login failed.');
});

Accounts.onCreateUser((options, user) => {
	const newUser = user;
	const userObj = {
		admin: options.profile.admin || false,
		email: options.email || null,
		first_name: options.profile.first_name || null,
		last_name: options.profile.last_name || null,
		photo: null,
		status: options.profile.status || null,
		role: null,
		work_title: options.profile.work_title || null,
		department: options.profile.department || null,
		location: options.profile.location || null,
		remote: options.profile.remote || null,
		country: options.profile.country || null,
		state: null,
		city: null,
		street: null,
		postal: null,
		apt_number: null,
		photo: null,
		company_mobile: options.profile.company_mobile || null,
		company_phone: options.profile.company_phone || null,
		company_phone_ext: options.profile.company_phone_ext || null,
		personal_mobile: null,
		personal_home: null,
		company_email: options.email || null,
		personal_email: null,
		emg_first_name: null,
		emg_last_name: null,
		emg_relationship: null,
		emg_work_phone: null,
		emg_work_phone_ext: null,
		emg_mobile_phone: null,
		emg_home_phone: null,
		emg_work_email: null,
		emg_personal_email: null,
		activated: options.profile.activated || true
	};

	Object.assign(newUser, userObj);
	return newUser;
});
