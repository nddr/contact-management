import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Meteor.startup(function () {
    if (Meteor.users.find().count() === 0) {
        // Create Admin account
        Accounts.createUser({
            email: 'admin@company.com',
            password: 'pass',
            profile: {
                admin: true
            }
        });
    }
});
