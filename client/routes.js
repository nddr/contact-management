import Vue from 'vue';
import Router from 'vue-router';
import { Meteor } from 'meteor/meteor';
import isMeteorUser from '../imports/helpers/meteor-user';
import isUserAdmin from '../imports/helpers/meteor-admin';

import Signin from '/imports/ui/views/Signin';
import Home from '/imports/ui/views/Home';
import Admin from '/imports/ui/views/Admin';
import CreateAdmin from '/imports/ui/views/CreateAdmin';
import UserDetails from '/imports/ui/views/UserDetails';
import EditUser from '/imports/ui/views/EditUser';

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'Signin',
            component: Signin
        },
        {
            path: '/home',
            name: 'Home',
            component: Home
        },
        {
            path: '/admin',
            name: 'Admin',
            component: Admin
        },
        {
            path: '/user/:id',
            name: 'UserDetails',
            component: UserDetails
        },
        {
            path: '/admin/create',
            name: 'CreateAdmin',
            component: CreateAdmin
        },
        {
            path: '/admin/edit/:id',
            name: 'EditUser',
            component: EditUser
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.path !== '/') {
        // If user isn't signed in, redirect to 'SignIn'
        isMeteorUser().then(response => {
            if (response) {
                if (to.name === 'Admin' || to.name === 'CreateAdmin' || to.name === 'UserDetails' || to.name === 'EditUser') {
                    // Make sure subscription is ready
                    isUserAdmin().then(reponse => {
                        if (Meteor.user().admin) {
                            next();
                        } else {
                            next(false);
                        }
                    });
                } else {
                    next();
                }
            } else {
                next({ name: 'Signin' });
            }
        });
    } else {
        next();
    }
});

export default router;
